<!DOCTYPE html>
<html lang="es">

<head>
    <title>Contador - ADLS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        @font-face {
            font-family: Arcade;
            src: url(PressStart2P-Regular.woff);
        }

        body {
            font-family: Arcade, Arial;
            margin: 0;
            background-color: black;
            color: white;
        }

        a {
            color: #AAAAAA;
        }

        .big-wrapper {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            overflow: hidden;
            position: relative;
            padding: .5rem;
        }

        .big-wrapper img {
            opacity: .2;
            position: absolute;
            top: 80px;
            width: 450px;
            height: auto;
        }

        @media (max-width: 475px) {
            .main-wrapper {
                margin: 0 0 2rem !important;
            }
        }

        .main-wrapper {
            flex: 1 0;
            display: grid;
            grid-template-columns: 1fr 1fr 1fr 1fr;
            grid-template-rows: repeat(14 2rem);
            max-width: 350px;
            margin: 0 4rem 2rem;
            column-gap: .3rem;
            row-gap: .3rem;
            position: relative;
        }

        .day-label {
            font-size: .6rem;
            grid-column-start: 1;
            grid-column-end: span 4;
            user-select: none;
            text-align: center;
        }

        .game-label {
            position: relative;
            font-size: 1.5rem;
            grid-column-start: 1;
            grid-column-end: span 4;
            user-select: none;
            text-align: center;
            margin-top: .5rem;
            margin-bottom: .2rem;
        }

        .game-name {
            grid-column-start: 1;
            grid-column-end: span 4;
            user-select: none;
            text-align: center;
        }

        .credits {
            font-size: 1.1rem;
            grid-column-start: 1;
            grid-column-end: span 4;
            user-select: none;
            text-align: center;
            margin-top: 1.5rem;
        }

        div[id^="coin-"] {
            width: 50px;
            height: 50px;
            margin-left: auto;
            margin-right: auto;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
            border: 2px solid white;
            border-radius: 50px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFUAAABVCAYAAAA49ahaAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpX5UHCwo4pChOogFURFHrUIRKoRaoVUHk0u/oElDkuLiKLgWHPxYrDq4OOvq4CoIgh8gbm5Oii5S4v+SQotYD4778e7e4+4dIFSLTLPaxgFNt81ELCqm0qti4BWd6EY/RgGZWcacJMXRcnzdw8fXuwjPan3uz9GjZiwG+ETiWWaYNvEG8fSmbXDeJw6xvKwSnxOPmXRB4keuKx6/cc65LPDMkJlMzBOHiMVcEytNzPKmRjxFHFY1nfKFlMcq5y3OWrHM6vfkLwxm9JVlrtMcQgyLWIIEEQrKKKAIGxFadVIsJGg/2sI/6PolcinkKoCRYwElaJBdP/gf/O7Wyk5OeEnBKND+4jgfw0BgF6hVHOf72HFqJ4D/GbjSG/5SFZj5JL3S0MJHQO82cHHd0JQ94HIHGHgyZFN2JT9NIZsF3s/om9JA3y3Qteb1Vt/H6QOQpK7iN8DBITCSo+z1Fu/uaO7t3zP1/n4AQWFyk77AIs8AAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQflCQsXLDvBH/KeAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAHkxJREFUeNrNfXuQFOW5/tOXuV93Z9ibu8AuJLBeIlYwF6O5KP5ISQhGiH9gihxAoYyWQaKlsFByOaIgciCigIgGS5KIyaZMWSSCCZdIVYipigEWWHZZWGB33cvszOzOTPf0THefP9yvzze9X/fMEnLOb6qmWGZ7uvt7vvf+Pm8vp+s6rvfr6tWr+P3vf489e/Ygk8lA0zTQ1+E4DhzHQRAE/OpXv0J9fb1/cHBwmq7rt3g8nls0TasRRbGB47iQpmlhAF5BEEQA+Xw+n9F1PaFpWjKXy3Xk8/luTdNODQ8Pnzp9+vSne/fuTfX09EDXdWiaBo7joOs6dF03riuKIhYuXIi5c+di4sSJ13393PUEtbe3F/v378fOnTuhqio0TTNADIfDmDhxItra2nDixAlHLBb7DoB7HQ7HPQBudTgcvCiKxuLJfXEcBwBg3Sf5TNd15HI55HI5LZvN/lOW5T9ls9lDr7zyymFd13OfffYZLly4gHw+b5yP53nwPI/Fixdj/vz5qKqq+v8LVEmS8OGHH2LdunVIp9OjJIPjOLS2tqKzs3OG1+t9SBCEB0RRDAqCUCC9BTdmApMGmgaV/swMsiRJQ5lMpnloaGjf9OnTP5o6dSo0TYPL5YIkSca9eTweNDU1Yfbs2XC73f+3oOq6jtbWVqxbtw7nzp3D8PAwAMDhcCCXy0EURXz88cehbDa72Ol0PuZ0OhtEUSwA0Awm/eJ53li4WVqJetNAWgGsKAqSyWTH8PDwq/v379/T3NycVFV11CZ+5StfQVNTE6ZOnfp/A2o2m8Wvf/1rbNq0CblcDrquQxAETJkyBVevXsVvf/vbsCiKT7nd7sc9Hk+I53lbIMlnHMcZYFqBxzIBLDDN/6qqiqGhoWR/f//2tra2zfv3708MDAygp6fH+L4gCHjuuecwd+5cOJ3O/z1QL1++jOeffx5HjhwxFsxxHGpqarBixQrHTTfd9ITX621yu91ltAmwU20COpE4WvLMdpa5EOo6rE2gz5fP5zE0NBTv7e19vqmp6eednZ052mTxPI/vfe97WLFiBaLR6L8f1L///e949NFHkUgkChbs8XjQ3Nz8zUAgsMvn802lpc1OxVn28V8xSeZrsswCeSuKgng8fu7s2bNLV61adUySJONYnucxefJkbNu2DV/4whf+PaDquo4PP/wQy5Ytg6qqBY7ovffe80QikY0+n+9xl8vF0ZJjdz7WolnH2ElpsQ2zc3DEtKRSKf3SpUvbFy1a9AwAiUi6IAhwu93YvXs3vvrVr15fUFVVxY4dO/DKK69AVVXwPI+qqir09/fj3XffvbGiomJ/IBC4iaiw1ULNam1nG1mf2YVXLJCLbSr9zuVy6O7ubmlra3tw//79Zzo7OzEwMAAAEEUR27dvx4wZM64PqLquo7m5GWvWrIGmaZAkCXV1dTh27BhaWloeqKysfNvj8fjsVN0c/Fs5lX/Hy8opmjWFOLJkMpnu6+tbMHfu3Gav1wtVVY1EZcuWLZg1a9a/Bqqu6zhw4ACefPJJaJpmZCgXLlzA2bNnn62urt5A1J21AJZkFgOSJZ1mh2U+3koirWJfK6klm5/JZPSOjo6VV69efXHVqlXGNYjE3nvvvdcO6tGjR/Hwww8bmREAdHR0cK2trf9VWVn5U4fDUeAY6JSQ5XhY4FrZWDvwzVphpyUsc8DaGHP4JssyLl68uO0b3/jGk5MmTdLJMYIg4Je//CWmT58+dlA//fRTLFmyBPF43LCjFy9e5E6fPr2jtrZ2qSAIBWGM+caKeXXWQjRNM9JHOvA3e3KiNfR3zd8ptgn0MVZJRTabxaVLl3Z9/etff7ShocE4KBwO47333kNDQ0PpoF6+fBk//OEPDUMNAFOmTMHLL7+89YYbbvgpyYrMN18swzEDSdsrh8MBq7SVThzMmkDOlc/nkc/noWkayIZbOU76cxa49IZls1lcuXJl25o1a5bFYjEMDAyA4zjceOON2Lt3L8rKykbhJ6xZs6bgg1Qqhaeffhqtra3QdR2TJk1CbW0tNm/e/GxVVVWTw+FgLtROOs0A5HI58DwPt9sNl8sFh8NRIGU8z0MQBAMcK/tMS7IgCBBFEQ6Hw7iG2d6aowfWuVipstfr/VpjY2P22LFjH/M8j2w2i1gshkQigW9961sFWIwCVdd17NixA83NzQZIkUgE27ZteyAaje5ixaA0oCww6Z3P5/PkJg3JZNk3VVWhqiry+bzxM63yZoDp1JaWfBLgW4Vm5ljYbG7oipbX673n1ltvPfW73/3urKZpEEURLS0tqKurQ2Njo7X6nzhxAosXLzYqOACwb9++xvr6+k98Pp+PVikrVWRJKCm5eTyegrye5XlLSQrsbCTr3LIsGwUeK9trBtVsvzVNQywWS1+6dOn2n/zkJ2eJFjgcDhw4cKDAvhqSmslksGzZMnR3dxsXOXjwoCcYDH4UDodvYDkNlg1lqbrL5RoFKLGp9E2Tz8jn5kWZN8AsuVbgi6IIURQhy3LRmJwFNDm3y+Vychz3HVmW32ptbc2T73R1dWHmzJmGTzDE7p133sGZM2eME1y4cAGSJG0sKyu7kSUFLFWkF03U1+fzwel0FoBJqzVxMORNPme9SznGvCHkvgVBgN/vNypsZpNi/pcGlggDz/OIRCI3/uAHP9jY0dFhbMThw4dx6NChQvXv7OzErFmzIEmScaK33nrrm1OnTj3i9Xo5s9qz7BsNNKlV+ny+guqTWSpZ0mcXw7KkyFzlMttXsxPRNA2yLENRFDidTsMcmGu45vIjvQnJZFI/e/bst9esWXMskUiA4ziEQiEcOnQI5eXl4HVdxy9+8Qtks1lwHIfJkydj5syZjurq6p1er5djSagVoEQKOY6D3+83bpZWaysJZKk0C3izWaA1w2xG6GNop0OiDlmWCySbtfmsjQoEAlxdXd3O2267zVFZWQld1zE0NITm5ubPr9HT04MPPvjAiO90XceDDz74RDQabbTy9FaAkjTW6/WOsp12bxaA9Gcs1bZKAsyqTL5DYmICkNPphMfjgSzLBZtqZbfNZqC8vLxx0aJFT5Buh6Zp2LRpE3p6eiBUVlbir3/9q/HlhoaGsvvvv/+3wWDQTWxRMTtKg0pLKL0gltrTsS0rsyFql8vlDFtqFVdalQdZ2RUNLsmcWBmcVYJAyoKqqn4lm82+3tLSIpOsLhqNQujt7UUulzNOsGHDhlX19fX/TxCEUd6aFe7QGU0gEDA2wQyoWR2tcm86nFFVFb29vXj77bexY8cOtLe3IxqNory8fFQIZAUe61pmW5zL5YwUuVj5kD6nKIruSCSSf//99/9MwqsTJ05AmDhxouGgFixYELrjjjt+FQgE3OYWhzmOpNU+l8vB7/eDpK8su0bbKLtCCS396XQaL774Ig4ePIhEIoH29nYcP34cd955J0Kh0Kh4kxWlWMWv9HdFUUQ6nS65dkBvjqqqt3Z0dOzo6urKGnWIRCJh2MKvfe1rD5eXl4fMKZ2VvSIS6nQ6DUDpjMgMrlWCYPXq6urC3/72twKbOjg4iJMnTxbdIJaJMvsBuv8fCASQyWSM69glJjTIoVAotHjx4ofJ2jVNA08MeDKZREVFxWN0McKuhEc7AhLYm0MPGlAWAMWqReSc5hqD1SKL1WdZPoCU80jdQFGUgt/ZpcUA4HQ6EQ6HH7tw4QJcLtfnoLrdbtx2221YsWLFjKqqqvpS0khyUUVR4PP5CuwuyyOXUt1nLbiiogJ33XVXwe9DoRBuvvlm5mKtbLRdSk2OEQQBgUAAkiTZRgMskxAMBusPHjw4o7y8HDzPQ+Q4Dr29vfjSl770kMfjGWXb7LImnueNCpNdYF+KmhJTkk6nkUqloKoqFEXBt7/9bcRiMfzjH/9AZWUlHnvsMfh8PiMUEkXRCOKtnAqLkFGgrpQt9Xg8BdEArS0sadV1HV6vF16v96F4PP6RpmkQM5kMwuGwIxqNPsCSUJYNJFIaCAQspbSYvTQDqygKDh48iFdffbWAOkSfs6+vDxs2bIDf70ckEoGmabjzzjsxe/Zs1NbWWkYCrLzevNkEJL/fj56enlEbRQuaOboYic0fOHv27JL6+vocDwAzZ868u6KiImhV5WG1dXVdH+XtrUpzxWyfpmk4ffo0Nm7ciHg8jmw2a7xzuZyRIKiqikwmg/7+fpw7dw5tbW3Yu3cvVq1ahcHBwZK6rHZ9NPI7l8tV1LaauWIejyf47rvv3q3rOviRKvYMt9vNbI2wAMrn83C5XKNiWJZBt8vZaVBbWlqQz+dtEwsr+37+/HmcO3euIK0shcjBUmue5xEMBjE8PGxspl3PjJzf7XbD5/PN4DgOfDabRW1t7T1WO8iS0mw2C4/HY2l7i1FzWBpBwplAIMC0x1b9LnJPJF0sVmO1izvNBRVWWmy+Pp0weL3eewYHB8FfvXrVHwwGb2U12Fi7pKoq6B6VlYTatYdZG5jNZgEAyWSS2YSzyuZoYpndJpbKaCHHOJ1OZLNZy9ScdR2fz3drVVWVn3/jjTemBQIBvlirmF4kTYe0kqpiCzAvmKTKBKBwOMyMY616TjRDz8pZFSO50eW/YDCIRCJh228zn9vlcvHr16+fxtfU1NzCogxanUhRFMOelqL2pXCdCBOP/Dxr1iwsWrTIMu7keR6PP/44pk2bZpyDcBDs2iRWKm/VryI1jGKaQo4d6bvdwrtcrlvMhQQ7vhMp7tp1Ja2ckxWl0iypdXV1CAQCzFCGfDZ58mRMmDDB+EwUxWtmqFh9j8TfpUgqOV6W5Vv4srKymlLIZET1Sc3VznEUWwRLcnK5XEGBw3wMHWwTm0dX7GlyB8thlQqoWVpJRFIKB2ykgF3Da5rWUKwfbs7H7WI8syRa1SnNC6clle7AEsDM2sTzvHGMOZsqlRdrRf+hTQq5r1Jo8CPraOA9Hk/YjkliV1FiZSXFHJIVwDTnlRRirPJ4AiKhlbM2bqyAstYliiKTN0BXucwvl8sVFgGESlWJYk7JjqRrxdYj73Q6XfC5OZuha7tE/fv6+oxjzdyusTpK1lp5noeiKEVTXVM4GRIFQfDa8ePtJNOOkGunWqzfK4oy6oZHWhYFnxGHJAhCQcBvFadeq7SSa5inWGh8zMIz4ne8PMdx4liIt1b8+VKdk5VtJcE/UWdiDnieB6me0WZCEASk02njfHTaXKw3VQqg9MRgKaEm9RZ5XdfzpQBonvywA7KUUMa8uFQqVSC1ZEGqqkKW5VElN8KVIv8noBZr3JUSGdClSNrslMgEz/Mjs56WWYL5XyuqebHY1MqZkHcqlTLUOBQKMaMNU/ZihF6EkMY6t53UFgslVVUddV6740d8QEbUdT2p63qw1EEGVhV8LDk1q4DsdDqxbNkyoz47adIkHD16dNSMKs2N8vl8WLp0KeLxOERRRDAYHBV2jcVhmT27pmnIZDJGEsJysixTwHFcUhyZSK5jtR9YoNDlOZanLzW/Ngf13//+9wsmRcySRqu60+mE1+vFj370owJzcK0gWkmdLMsFpF6WE6axGOEoJHhVVTvMHFO7QoMZ1GJOwM6+EUBI083hcEAUxQKvy3EcqqqqCjig5DtOpxMulwsul8sypLpWQEk7h9Wmsco2R0Dt4CVJ6ja3ZO2kinQczQVcO3Ng5zxYdpd4f8KovvfeezFv3jxDvV0uV8FsAItUdq0vuiNM+GV2wkYX0EeYNN28oiinzCQtu6De5/NhaGioIKOwk8xijoLl0IgqkZff7wfdmfD7/SVNplyLc6KpRqR9X0pWSfp2HMed4nO53ClSjLUDli7eEracVVhlR/UutXpFT6oQe0muRyiadupud26rMIrUHPL5PPr6+gx6kVWZ0EwqGaFonuIFQfhUlmXNnBba2dVsNlvAQy01Zi1ma+nFKopiVMSIrSVVKpZHLkUiSxlOI6AODg7C5/MxHRJ9HropKUmSJknSp3xbW1tKUZR/stjHVjfm9/uN+SqLsKJgh63IZMUyIFKFcrlcBsGBmIOxtEqKPTKEVnui+ul02kh9rYSMBlRRFGSz2X/u2bMnxb/wwguQZflP5KZLMQFerxfxeLxgoWOVGistoDM34tHJGA85V6k21SoZYDUOCdEul8vhypUrqKysZA7GsRwaATWXy/2pq6vrcyZ1Op3+SJZlJqis1oEgCMjn88hms6NCrOsxvEtLDwmd8vk8XV6zzPjGElaZbSKR0u7ubmPjWDVjc+s8n8+TbvBHmqZ9DmooFPpzOp0eolnNxexqJBLB1atXC8i4xXrjxSrxtKmgVZEubgiCYAxmjGUM3cqG0t4+m82iq6sL4XAYZn6ulXQTr59Op4fC4fCfAXxOpnjkkUdykiQ1m02AlS0iBQxJkpDJZJiELhaholgOTnt40goHAK/XWxD0m+k4pQ76sjrDZOYgl8tBURRcuHAB0Wh01CCFeVPImonGyrLc3NTUlOM4DnwkEgHP85AkaR8hNFhJq7nPXllZiY6ODsNjXgupwqqHLkmSkZISySQLNfejxtKLMs8S5PN5KIoCWZbR3t6OyspKWM3e0vxbmlOWTqehado+0r7nBwcHIcsy7rjjjo+Gh4cvEnW2c0AkfnS73XA4HOjt7S0YuC2VAmRVXyDn9vl88Hg8CAQCcLlc8Pl8RlpqFafSxQ47wh0xLyNeG8PDw+jq6kJZWVlBT4wl7QQfsiGpVOri/PnzP0omk5/XLSZMmIB8Pg9BELBnz56f1dXVbSazoyRNpONX2lMqioJMJoP29nbcfPPN8Pl8xqCuOXW0GkiwSv0URYEkSVBVFT6fD7lcDslk0ohTid0bC1fKlKMbEprJZPDxxx9j0qRJCAaDcDqdcDgccLvdcLvdBbNWROjy+TwkSUI8Hkdvb+9TP/vZz142HrZA24jy8vI3ksnkaqfTGbIa66YljjiN2tpanDx5El/+8pcLmCKkb24eHrMqctP8JCKR5OV2uwu4AHZ5vlV3ggWoJEloaWlBNBqFz+crsNlmYTB7fEVRMDQ0lOQ47g363nmy27quY86cOcl0Or2djBnaZU30CLnX60U0GsWZM2eMYgttRsw801IzLnIN+s3yylbSzxrrVBTFUHlJktDZ2YlMJoOKigoDUNrEWTknoqWyLG9fvnx5ssA00vZpxOO+nEwmEwQU1rwTvWhStisrKwPHcWhrazPiVxpYMyWy1F6WVfdgLM6IBoKYlXQ6je7ubnR2dmLixIlwOp0FD1+gq19E7enzZLNZJBKJhCAIL9OA3nfffeCXL19eoJqPPPJIPJlM/icdKrFSWLO0Op1OVFdXQ5IktLe3G4RdO3BLSRZKnRWw4rXS6k6kM5PJoLu7G62trfjiF79o1GNpP0A7K3rqkIRfqVQKmUzmP9evXx+n4+vFixeDnz17tsE1JSW2fD7/88HBwbN03dRsBmhpIc7J5XKhrq4OmUwGLS0tyGazJH0rmNorxrwuFTgWu5seDyLXlWXZsJ/pdBoXL17E+fPn0djYCI/HU/BAB7OJYam9LMuIx+NnVVX9OS1ks2bNwvTp0yG89NJLCIVCOHLkiPGwgT/+8Y/a3XfffcrhcPyH0+nkSnUKRAX8fr8RFUQiEdv8uRg/yRwj2rFnzNJJsiSi8plMBidPnkQikcDUqVPh8XiMZwHQNpuATNJxs/mIx+N6Mpmct3LlyoukbCoIArZu3YqamprP5/3nzJmD8ePHg+M4wwBHo9FjsVhsuyRJxpihFXeK3mFRFOFyuVBTU4OamhocP34cvb29kCSpwEkoilIw58+aELQaLqMLGfRzAMj5iVQSIIeGhjAwMIAjR47A6XRiypQpox45Qrd2yM80oETT0uk0EonE9qGhoWN0G3vx4sX/Q+0ku3348GEsXLiwgDu0a9cuTzAY/HtlZeWNbrfb2D1zZ5X1dAlyI5lMBm1tbXA6nWhsbDRiQKJi5rjWqrBs1cE0hznkTdReURS0tbWhu7sbjY2NCIVCBdc3PwiH9Mho1Sc2WZIk9PT0nJEkaXpTU5NETabg+PHjqK6uLgRVVVU0NTXhnXfeKWi67dy5szEajX5SXl7uI5ECDaxZqszSRlRwcHAQ7e3tqK6uRkNDg6F2ZHGsnpNd2ESHSmYnQqKP7u5unD9/HjfccAOqq6vh8XgKNpEOnehNJmszO7n+/v50PB6/ffXq1Wfp7u6uXbswe/bs/xEC2ob19vbiu9/9Lvr6+gygXC4XXnvttQfGjRv3m2AwyJHQw4pSSRbKsnGyLKO/vx+dnZ2IRCKor68HrQG0sygGKksr6HpoZ2cnxo0bh7q6Oni93oINtIqDzWP4dPgUj8f1wcHBeZs2bWomjznlOA4PPvggNm/eDPJoqVGgAsBf/vIX/PjHPwapr44fPx7t7e3Yt2/fs+PGjXshEAiMukGzkzHPpNIAELs3ODiIixcvguM4RKNRVFVVGc84KZbammNQRVHw2WefoaenB6lUCuPHj0dFRcUoz86qu7KSCfO9JpNJxGKxFQsXLnxx8uTJGB4eBsdxqK6uxoEDB0Y9JHwUp/uuu+7CM888g/Xr10PXdfT395MxnBf7+/urAPyULN5McaRtIN2KoG0XqTy53W5Eo1HIsozBwUG0tLQglUqRkUSjiEIeBkbKgMQBDQ0NYXh4GOl0Gqqqorq6GpMnT4bX6zUCefOT1KzIyCyBIKHg8PAwBgYGtuVyuRdra2uNx0vxPI/XXnuN+dR15mPpZFnG8uXL8f777xeQcfft28flcrkdkUhkqc/nMxZbipraPU7OXNPMZDJIp9PIZDJGEkEk2OFwwOPxwOPxGADSJAyz07NKsa2G5GgJHZku3JXNZh9dvXq1TgvMpk2b8NBDD7HDP6u4MBaLYdGiRfjkk08KKu9vvvkmp+v6f0UikZ/6/f6CTmexlNFqeo4O3M3Ozm5uySyFxapWVrUG+vrEhqbTaSKhT9KAksmYZ555hsmJtQUVAK5cuYJ58+bh8uXLBYs7dOgQOjs7ny0vL98QCAQ4WlKsapA0uHb0GRaV0RxKFaNnjpVrS1fwR+qjen9//8qjR4++SJ43RTZxwYIFWLt2re3fAyj6pN/z589j/vz56O7uNgCpqKjA6tWrIYriA+Fw+O1QKOQj1MZiD/hmTfAVY3+wMi9WCbFYT4p1jDlsymQy6b6+vgWJRKJ5586dyGQyhum5//77sXnz5gI+wDWBCgCtra1YsGABrly5Ak3TEAwGkc/n4XA4sGHDhht9Pt/+srKym9xud0HaZzW1UkxKSwXKruhiN1pPfiYhGHFKQ0NDLb29vQ9u3rz5DOH7E6bMvHnzjLH4omzGUlXl0qVLWLJkCU6fPj2KB79lyxaP3+/fGAqFHvf7/ZzZ+7IK3GP5kx2lzh6wnubGst901jWSLuuxWGz7wMDAMxs3bpSIZJJ/Fy1ahJUrVxoNyOsGKgD09/fj2WefxR/+8IeCmx559BL8fv83/X7/rlAoNJXOXqwGx6xa1KUUW8xPHbKrYNFFZrpOoOs6UqnUua6urqXpdPrY1q1bRyUGK1euxJIlS8b01ynG/McTUqkUtmzZgtdff31Ug9DpdGLNmjWOYDD4hN/vbwoEAmWkVkmno2P9owrF2iSsoos57qSlk+M4yLIcHxgYeD4Wi/18+/btOfIYKTqmfv3113HfffeNmZ55TX/mQ1VVfPDBB3jiiSeMHdd1HdFo1Jgyee6558Iej+cpn8/3uM/nC9HpLauyXqwobfWwW5Z9NhdYCLAjrfhkIpHY3tfXt3nr1q0JURQhSVJBL23atGnYsmXLqIfN/ltBJa+2tjasXbsWhw8fNuwsYUHrug632421a9eGvF7vYq/X+5jX620g7RczWbcYH4BlN+2e60d+JueWJKkjHo+/GovF9rzwwgtJMjZEmx2e5/Hkk09i6dKlCAaD14zLv/z3qGRZRnNzM1avXm3suLnmCgBPPfUUJkyYMMPlcj3kdrsfcLlcQcKVYoFa7PHxrPYMLbkjceeQLMvNsVhsX3d390e7d+9mxraCIGDatGlYt24dbr/99n+ZC3bd/nJaV1cXdu3ahbfeemsUWwUAKisrkU6nEQ6HsXTpUkckEvlOIBC4V9O0eziOu1UURZ5+XJEZYDq5YD2nWlVVpNNpTZblf2YymT+lUqlD6XT68O7du3PkfshIpOlZ03j++ecxZ86ckr37/xqo5HXq1Cm8+eab+M1vflNAtXQ6nQX9HsLee/rpp6Fpmt/v90/z+/23CIJwi8PhqHG73Q0cx4V4ng/ruu5VFEVUFCWvaVpGVdWEqqpJnuc7AHSrqnqqt7f3VC6X+5TjuNRLL71kmASS+RAHRcAUBAGrV6/G3LlzMW7cuOuKwX8Dg7tOSjROnagAAAAASUVORK5CYII=);
        }

        .coin-used {
            border: 2px solid red !important;
        }

        #help {
            text-decoration: none;
        }

        #help::before {
            content: '(?)';
        }

        #help.active::before {
            content: '(X)';
        }

        #help-container {
            display: none;
            position: absolute;
            font-size: 0.5em;
            padding: 1rem;
            background-color: #222222f0;
            border-radius: 1rem;
        }

        #help-container.active {
            display: block;
        }
    </style>
    <script>
        function setCoins(amount) {
            const total = 28;
            if (amount == total) {
                amount = 0;
            }
            localStorage.setItem("coins", amount);
            document.getElementById("counter").innerHTML = `${amount} / ${total}`;
            for (let i = 1; i <= total; i++) {
                document.getElementById("coin-" + i).className = i <= amount ? "coin-used" : "";
            }
        }

        var current_game = <?php echo json_encode(urldecode(file_get_contents('current_game.html'))) ?>;

        window.onload = function () {
            document.getElementById("game-name").innerHTML = current_game;

            // Init game
            if (localStorage.getItem("game") === null && current_game !== '') {
                localStorage.setItem("game", current_game);
            }

            // Reset counter when game changes
            if (
                localStorage.getItem("game") !== null &&
                current_game !== '' &&
                localStorage.getItem("game") !== current_game
            ) {
                localStorage.setItem("game", current_game);
                localStorage.setItem("coins", 0)
            }

            if (localStorage.getItem("coins") !== null) {
                setCoins(localStorage.getItem("coins"));
            }

            document.getElementById("help").addEventListener("click", function (e) {
                e.preventDefault();
                let el = e.target;
                let help = document.getElementById("help-container");
                const marker = "active";
                el.classList.toggle(marker);
                help.classList.toggle(marker);
            });
        };
    </script>
</head>

<body>
    <div class="big-wrapper">
        <img src="adls_logo.webp" alt="ADLS logo" />
        <div class="main-wrapper">
            <div class="game-label">
                ADLS <a href="#" id="help" title="Ayuda"></a>
                <div id="help-container">
                    Haz doble click sobre un botón para registrar tus partidas.
                    <br />
                    <br />
                    Contador de partidas del salón de Telegram <a href="https://t.me/+Ucbiyh0QSDo6lBt1">ADLS</a>.
                </div>
            </div>
            <div id="game-name" class="game-name"></div>

            <div class="day-label">Lunes</div>
            <div id="coin-1" ondblclick="setCoins(1)" class=""></div>
            <div id="coin-2" ondblclick="setCoins(2)" class=""></div>
            <div id="coin-3" ondblclick="setCoins(3)" class=""></div>
            <div id="coin-4" ondblclick="setCoins(4)" class=""></div>

            <div class="day-label">Martes</div>
            <div id="coin-5" ondblclick="setCoins(5)" class=""></div>
            <div id="coin-6" ondblclick="setCoins(6)" class=""></div>
            <div id="coin-7" ondblclick="setCoins(7)" class=""></div>
            <div id="coin-8" ondblclick="setCoins(8)" class=""></div>

            <div class="day-label">Miércoles</div>
            <div id="coin-9" ondblclick="setCoins(9)" class=""></div>
            <div id="coin-10" ondblclick="setCoins(10)" class=""></div>
            <div id="coin-11" ondblclick="setCoins(11)" class=""></div>
            <div id="coin-12" ondblclick="setCoins(12)" class=""></div>

            <div class="day-label">Jueves</div>
            <div id="coin-13" ondblclick="setCoins(13)" class=""></div>
            <div id="coin-14" ondblclick="setCoins(14)" class=""></div>
            <div id="coin-15" ondblclick="setCoins(15)" class=""></div>
            <div id="coin-16" ondblclick="setCoins(16)" class=""></div>

            <div class="day-label">Viernes</div>
            <div id="coin-17" ondblclick="setCoins(17)" class=""></div>
            <div id="coin-18" ondblclick="setCoins(18)" class=""></div>
            <div id="coin-19" ondblclick="setCoins(19)" class=""></div>
            <div id="coin-20" ondblclick="setCoins(20)" class=""></div>

            <div class="day-label">Sábado</div>
            <div id="coin-21" ondblclick="setCoins(21)" class=""></div>
            <div id="coin-22" ondblclick="setCoins(22)" class=""></div>
            <div id="coin-23" ondblclick="setCoins(23)" class=""></div>
            <div id="coin-24" ondblclick="setCoins(24)" class=""></div>

            <div class="day-label">Domingo</div>
            <div id="coin-25" ondblclick="setCoins(25)" class=""></div>
            <div id="coin-26" ondblclick="setCoins(26)" class=""></div>
            <div id="coin-27" ondblclick="setCoins(27)" class=""></div>
            <div id="coin-28" ondblclick="setCoins(28)" class=""></div>

            <div class="credits">CREDITS: <span id="counter"></span></div>
        </div>
    </div>
</body>

</html>